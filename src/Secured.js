import React, { Component } from "react";
import Keycloak from "keycloak-js";
import axios from 'axios';


class Secured extends Component {
    constructor(props) {
        super(props);
        // const keycloak = Keycloak("/keycloak.json");
        this.state = { keycloak: null, authenticated: false };
    }

    componentDidMount() {
        const keycloak = Keycloak('/keycloak.json');
        keycloak.init({ onLoad: "login-required" }).then((authenticated) => {
            if (authenticated) {
                window.localStorage.setItem("token", keycloak.token);
                window.localStorage.setItem("refreshtoken", keycloak.refreshToken);
            }
            this.setState({ keycloak: keycloak, authenticated: authenticated });
        });
    }

    handleLogin = () => {
        axios.get("https://s2.videostream.ninja:9001/fruit", {
	   // withCredentials: true,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token'),
            }
        }).then(function (res) {
            console.log(res.data);
   	    console.log(res.status);
            console.log(res.statusText);
            console.log(res.headers);
            console.log(res.config);
        })
            .catch(function (err) {
                alert(err)
            })
    }

    render() {
        if (this.state.keycloak) {
            if (this.state.authenticated)
                return (
                    <div>
                        <p>
                            This is a Keycloak-secured component of your application. You
                            shouldn't be able to see this unless you've authenticated with
                            Keycloak.
            </p>
                        <button onClick={this.handleLogin}>login</button>
                    </div>
                );
            else return <div>Unable to authenticate!</div>;
        }
        return <div>Initializing Keycloak...</div>;
    }
}
export default Secured;
